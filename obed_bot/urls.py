from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from telegram.views import index

urlpatterns = [
    path('obed/admin/', admin.site.urls),
    path('obed/bot/', index)
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
