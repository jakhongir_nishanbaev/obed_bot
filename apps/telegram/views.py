from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from telegram.models import TelegramUser
from .bot import Start, Control

# token
from obed_bot.settings import BOT_TOKEN

# telebot
import telebot

bot = telebot.TeleBot(BOT_TOKEN)


@csrf_exempt
def index(request):
    if request.method == 'GET':
        return HttpResponse("Hello, this is telegram bot page")
    if request.method == 'POST':
        bot.process_new_updates(
            [telebot.types.Update.de_json(
                request.body.decode("utf-8"))
            ]
        )
        return HttpResponse(status=200)


@bot.message_handler(commands=['start'])
def start(message):
    Start(user_id=message.from_user.id,
          username=message.from_user.username,
          first_name=message.from_user.first_name,
          last_name=message.from_user.last_name
          ).check_user()


@bot.message_handler(content_types='text')
def text_handler(message):
    text = message.text
    user_id = message.from_user.id
    message_id = message.message_id
    check_user = TelegramUser.objects.filter(user_id=user_id).exists()
    handler = Control(user_id=user_id, message_id=message_id)
    if check_user:
        user = TelegramUser.objects.get(user_id=user_id)
        if text == "Назад ↩️":
            handler.back_button_handler()
        elif text == "🚖 Оформить заказ":
            handler.checkout_order()
        elif text == "📄Оставить отзыв":
            handler.send_feedback()
        elif "🛒Корзина(" in text:
            handler.open_basket()
        elif text == "🔄 Очистить" and user.step == 5:
            handler.clear_basket()
        elif text == u'Список' and user.step == 2:
            handler.send_product_list()
        elif '❌ ' in text:
            handler.delete_in_basket(text)
        elif user.step == 1:
            handler.save_category(category=text)
            handler.product()
        elif user.step == 2:
            handler.save_product(product=text)
        elif user.step == 3:
            handler.save_quantity(quantity=text)
        elif user.step == 6:
            handler.save_address(address=text)
        elif user.step == 7:
            phone = message.text
            handler.save_contact(phone)
        elif user.step == 9:
            handler.save_payment(text)
        elif user.step == 11:
            handler.send_channel_feedback(message=message)
        elif user.step == 12:
            handler.send_client_feedback(text)


@bot.callback_query_handler(func=lambda call: True)
def inline_button_handler(call):
    call_data = call.data
    user_id = call.from_user.id
    message_id = call.message.message_id
    check_user = TelegramUser.objects.filter(user_id=user_id).exists()
    handler = Control(user_id=user_id, message_id=message_id)
    if check_user:
        user = TelegramUser.objects.get(user_id=user_id)
        if call_data == 'success_order':
            handler.success_orders()
        elif call_data == 'cancel_order':
            bot.delete_message(chat_id=user_id, message_id=message_id)
            handler.open_basket()
        elif call_data.startswith('reply'):
            handler.reply_feedback(call_data)
        elif call_data.startswith('yes_order_kitchen'):
            text = call_data.split("%")
            handler.channel_update_status(order_id=text[1], message_id=message_id, accepted=True,
                                          call=call)
        elif call_data.startswith('no_order_kitchen'):
            text = call_data.split("%")
            handler.channel_update_status(order_id=text[1], message_id=message_id, accepted=False, call=call)
        elif call_data.startswith("yes_order_driver"):
            text = call_data.split("%")
            handler.send_driver_confirmation(order_id=text[1], message_id=message_id, call=call)
        bot.answer_callback_query(callback_query_id=call.id)


@bot.message_handler(content_types='location')
def location_handler(message):
    user_id = message.from_user.id
    message_id = message.message_id
    check_user = TelegramUser.objects.filter(user_id=user_id).exists()
    handler = Control(user_id=user_id, message_id=message_id)
    if check_user:
        user = TelegramUser.objects.get(user_id=user_id)
        if user.step == 6:
            handler.save_address(message.text,
                                 longitude=message.location.longitude,
                                 latitude=message.location.latitude)


@bot.message_handler(content_types='contact')
def contact_handler(message):
    user_id = message.from_user.id
    message_id = message.message_id
    check_user = TelegramUser.objects.filter(user_id=user_id).exists()
    handler = Control(user_id=user_id, message_id=message_id)
    if check_user:
        user = TelegramUser.objects.get(user_id=user_id)
        if user.step == 7:
            handler.save_contact(message.contact.phone_number)


@bot.channel_post_handler(content_types=['text'])
def channel_handler(message):
    if message.text == 'id':
        bot.send_message(message.chat.id,str(message.chat.id))