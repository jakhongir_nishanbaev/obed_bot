import requests
from datetime import datetime
import hashlib


def call_uzclick(data):
    return requests.post('https://merchant.click.uz/api/', data=data)


def call_uzclick_invoice(secret_key, service_id, merchant_id,
                         phone_number, order_id, user_id, amount):
    sign_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    md5 = hashlib.md5()
    md5.update(sign_time.encode())
    md5.update(secret_key.encode())
    md5.update(service_id.encode())
    md5.update(order_id.encode())
    md5.update(amount.encode())

    token = md5.hexdigest()
    req = {
        'action': 'invoice',
        'service_id': int(service_id),
        'merchant_id': int(merchant_id),
        'merchant_user_id': int(user_id),
        'secret_key': secret_key,
        'sign_time': sign_time,
        'sign_string': token,
        'phone': phone_number,
        'amount': amount,
        'transaction_param': order_id,
        'transaction_note': 'Оплата заказа #{}'.format(order_id)
    }

    r = call_uzclick(req)

    data = r.json()
    return data['invoice_id']