from django.db import models


class TelegramUser(models.Model):
    user_id = models.CharField(max_length=50)
    username = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    feedback = models.CharField(max_length=255, blank=True, null=True)
    step = models.IntegerField(default=1)

    def __str__(self):
        return '{} | {}'.format(str(self.user_id), self.first_name)
