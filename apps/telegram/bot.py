# timezone
import re

from django.db.models import Q
from django.utils.timezone import localtime, localdate, timedelta

# token
from obed_bot.settings import BOT_TOKEN, SERVER_URL
from .click import call_uzclick_invoice

# telebot
import telebot
from telebot.types import (
    ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardButton,
    InlineKeyboardMarkup, ReplyKeyboardRemove
)

# model
from category.models import Category
from product.models import Product
from order.models import OrderProduct, Order
from .models import TelegramUser

bot = telebot.TeleBot(BOT_TOKEN)


class Start:
    def __init__(self, user_id=None, username=None, first_name=None, last_name=None):
        self.user_id = user_id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name

    def check_user(self):
        filter_user = TelegramUser.objects.filter(user_id=self.user_id).exists()
        if filter_user:
            get = TelegramUser.objects.get(user_id=self.user_id)
            get.first_name = self.first_name
            get.last_name = self.last_name
            get.save()
        else:
            TelegramUser.objects.create(user_id=self.user_id, first_name=self.first_name,
                                        last_name=self.last_name, username=self.username)
        if self.first_name is not None:
            text = "Привет " + str(self.first_name) + "!, Я бот службы доставки  Давай пообедаем!\n Что пожелаете?"
            bot.send_message(self.user_id, text)
        Control(user_id=self.user_id).category()


class Control:
    def __init__(self, user_id, message_id=None):
        self.user_id = user_id
        self.message_id = message_id
        self.channel_di = -1001450970329
        self.deliver_channel_di = -1001373259061
        self.feedback_channel_id = -1001178976777
        self.user, create = TelegramUser.objects.get_or_create(user_id=self.user_id)

    def back_button_handler(self):
        step = self.user.step
        if step == 2:
            self.category()
        elif step == 3:
            self.product()
        elif step == 5:
            self.category()
        elif step == 6:
            self.open_basket()
        elif step == 7:
            self.send_product_address()
        elif step == 8:
            self.check_order_products()
        elif step == 9:
            self.send_contact()
        elif step == 11:
            self.category()
        elif step == 22:
            self.product()

    def category(self):
        self.new_step(1)
        key = ReplyKeyboardMarkup(True, False, row_width=2)
        category = Category.objects.all()
        buttons_array = []
        for i in category:
            buttons_array.append(KeyboardButton(i.name))
        key.add(*buttons_array)
        key.add(self.basket_btn(), KeyboardButton('🚖 Оформить заказ'))
        key.add(KeyboardButton('📄Оставить отзыв'))
        text = 'Выберите категорию'
        bot.send_message(self.user_id, 'Отдел доставки работает c 10:00 до 20:00')
        bot.send_message(self.user_id, text, reply_markup=key)

    def save_category(self, category):
        filter_category = Category.objects.filter(name=category).exists()
        if filter_category:
            order_product = self.get_order_product()
            order_product.category = category
            order_product.subcategory = None
            order_product.save()

    def product(self):
        self.new_step(2)
        key = ReplyKeyboardMarkup(True, False, row_width=2)
        order_product = self.get_order_product()
        category = self.get_category(order_product.category)
        product = Product.objects.filter(category=category, is_active=True)
        product_array = []
        for i in product:
            product_array.append(KeyboardButton(i.name))
        key.add(self.back_btn(), KeyboardButton(u'Список'))
        key.add(*product_array)
        key.add(self.basket_btn())
        text = "Нажмите « ⏬ Список » для ознакомления с меню или выберите блюдо"
        bot.send_message(self.user_id, text, reply_markup=key)

    def save_product(self, product):
        filter_product = Product.objects.filter(Q(name=product, is_active=True) |
                                                Q(command_name=product.replace("/", ""), is_active=True)).exists()
        if filter_product:
            order_product = self.get_order_product()
            category = self.get_category(order_product.category)
            get = Product.objects.get(category=category, name=product, is_active=True)
            order_product.product = get
            order_product.save()
            self.open_product(order_product)

    def open_product(self, order_product):
        self.new_step(3)
        row = []
        des = f'{order_product.product.description[:70]}...' \
            if len(order_product.product.description) > 70 else\
            order_product.product.description
        key = ReplyKeyboardMarkup(True, False, row_width=3)
        text = f'<b>{str(order_product.product.name)}</b>' + '\n<b>Цена:</b> ' + str(order_product.product.price)\
               + f' сум \n<i>{des}</i>'

        for i in range(9):
            row.append(KeyboardButton(str(i + 1)))
        key.add(*row)
        key.add(self.back_btn())
        bot.send_photo(chat_id=self.user_id, caption=text, photo=order_product.product.photo, parse_mode='HTML')
        bot.send_message(self.user_id, '<b>Выберите</b> или <b>введите</b> количество:', reply_markup=key,
                         parse_mode='HTML')

    def send_product_list(self):
        order_product = self.get_order_product()
        category = self.get_category(order_product.category)
        products = Product.objects.filter(category=category, is_active=True)
        for product in products:
            text = f"{product.name} \n\nЦена: {product.price} Цена \n\nДобавлять: /{product.command_name}"
            bot.send_message(chat_id=self.user_id, text=text)

    # def get_product_command(self, command):
    #     isExist = Product.objects.filter(command_name=command).exist()
    #     if isExist:
    #         chosen_product = Product.objects.get(command_name=command)
    #         self.save_product(chosen_product, command=command)

    def save_quantity(self, quantity):
        if self.check_int(quantity):
            order_product = self.get_order_product()
            order_product.quantity = int(quantity)
            order_product.save()
            self.add_basket()
        else:
            bot.send_message(self.user_id, 'Введите количество в <b>цифрах</b>', parse_mode='HTML')

    def add_basket(self):
        order_product = self.get_order_product()
        order = self.get_order()
        check = OrderProduct.objects.filter(order=order, category=order_product.category, product=order_product.product,
                                            status='order').exists()
        if check:
            get = OrderProduct.objects.get(order=order, category=order_product.category, product=order_product.product,
                                           status='order')
            get.quantity += order_product.quantity
            get.save()
        else:
            order_product.status = 'order'
            order_product.save()
            create = OrderProduct.objects.create(order=order, category=order_product.category, status='new')
        bot.send_message(self.user_id, 'Добавлено в корзину\nХотите что-то еще?')
        self.product()

    def open_basket(self):
        order = self.get_order()
        order_product = OrderProduct.objects.filter(order=order, status='order')
        if not order_product:
            bot.send_message(self.user_id, 'Ваша корзина пуста, выберите что-нибудь для заказа')
            if self.user.step == 5:
                self.category()
        else:
            self.new_step(5)
            key = ReplyKeyboardMarkup(True, False, row_width=2)
            total_price = 0
            buttons_array = []
            text = "Корзина:\n\n"
            for i in order_product:
                price = i.quantity * i.product.price
                total_price += price
                text += '<b>' + str(i.product.name) + '</b>' + '\n' + str(i.quantity) + \
                        ' * ' + str(i.product.price) + ' = ' + str(price) + '\n\n'
                buttons_array.append(KeyboardButton('❌ ' + str(i.product.name)))
                i.delete = str(i.product.name)
                i.save()
            text += '<b>Итого: ' + str(total_price) + '</b>'
            key.add(self.back_btn(), KeyboardButton('🔄 Очистить'))
            key.add(*buttons_array)
            key.add(KeyboardButton('🚖 Оформить заказ'))
            send_text = "«❌ Наименование » - удалить одну позицию\n«🔄 Очистить » - полная очистка корзины"
            bot.send_message(self.user_id, send_text)
            bot.send_message(self.user_id, text, reply_markup=key, parse_mode='HTML')

    def checkout_order(self):
        order = self.get_order()
        order_product = OrderProduct.objects.filter(order=order, status='order')
        if not order_product:
            bot.send_message(self.user_id, 'Ваша корзина пуста, выберите что-нибудь для заказа')
        else:
            self.send_product_address()

    def send_feedback(self):
        self.new_step(11)
        key = ReplyKeyboardMarkup(True, False)
        key.add(self.back_btn())
        text = "Здравствуйте " + str(self.user.first_name) \
               + "!\n\n<b>Напишите</b> свои отзывы, пожелания, комментарии по улучшению сервиса или предложения" \
                 " о сотрудничестве прямо сюда и мы обязательно с Вами свяжемся.\n\nС уважением, команда <b>Давай пообедаем</b>"
        bot.send_message(self.user_id, text, reply_markup=key, parse_mode='HTML')

    def send_channel_feedback(self, message):
        text = message.text
        inline = InlineKeyboardMarkup()
        di = "reply" + str(message.chat.id) + '%' + str(message.message_id)
        inline.add(InlineKeyboardButton('Ответить', callback_data=di))
        bot.send_message(self.feedback_channel_id, text, reply_markup=inline)
        self.category()

        def reply_feedback(self, callback):
            self.new_step(12)
            print(callback)
            sp = callback.split('reply')
            self.user.feedback = sp[1]
            self.user.save()
            bot.send_message(self.user_id, ' Напишите ответ!')

        def send_client_feedback(self, text):
            sp = self.user.feedback.split('%')
            bot.send_message(sp[0], text, reply_to_message_id=sp[1])
            bot.send_message(self.user_id, 'Отправлено ✅')
            self.category()

    def send_product_address(self):  # delivered address
        self.new_step(6)
        btn_text = 'Отправить локацию'
        send_text = 'Введите адрес доставки:'
        key = ReplyKeyboardMarkup(True, False)
        # if self.user.address is not None:
        #     key.add(KeyboardButton(str(self.user.address)))
        button = KeyboardButton(request_location=True, text=btn_text)
        key.add(button)
        key.add(self.back_btn())
        bot.send_message(self.user_id, send_text, reply_markup=key)

    def save_address(self, address=None, latitude=None, longitude=None):
        order = self.get_order()
        order.latitude = latitude
        order.longitude = longitude
        order.address = address
        order.save()
        self.send_contact()

    def send_contact(self):
        self.new_step(7)
        btn_text = 'Отправить контакт'
        send_text = '<b>📱 Отправьте</b> или <b>введите</b> ваш номер телефона в формате:\n+998 ** *** ** **\n'
        key = ReplyKeyboardMarkup(True, False)
        button = KeyboardButton(request_contact=True, text=btn_text)
        key.add(button)
        key.add(self.back_btn())
        bot.send_message(self.user_id, send_text, reply_markup=key, parse_mode='HTML')

    def save_contact(self, contact):
        if len(contact) < 12:
            send_text = '<b>📱 Отправьте</b> или <b>введите</b> ваш номер телефона в формате:\n' \
                        '+998 ** *** ** **\n' \
                        '<b>Примечание:</b> Если вы планируете оплатить заказ онлайн с помощью Click,' \
                        ' пожалуйста, укажите номер телефона, на который зарегистрирован аккаунт в данном сервисе!'
            bot.send_message(self.user_id, send_text, parse_mode='HTML')
        else:
            self.user.phone = contact
            self.user.save()
            self.select_payment_type()

    def select_payment_type(self):
        self.new_step(9)
        row = []
        key = ReplyKeyboardMarkup(True, False, row_width=2)
        payment = ['💵 Наличные']
        for i in payment:
            row.append(KeyboardButton(str(i)))
        key.add(*row)
        key.add(self.back_btn())
        text = 'Способ оплаты:'
        bot.send_message(self.user_id, text, reply_markup=key)

    def save_payment(self, payment):
        if payment in ['💵 Наличные']:
            order = self.get_order()
            order.payment = payment
            order.save()
            self.check_order_products()

    def pay_click(self, order):
        secret_key = 'sewp7GSIe04gcFu27k'
        merchant_id = 9360
        user_merchant_id = 12816
        service_id = 41104
        total = self.order_total_amount(order)
        payment_text = ''
        phone_number = self.user.phone.replace('+', '')
        amount = '{0:.2f}'.format(total + 10000)
        try:
            invoice_id = call_uzclick_invoice(
                secret_key=str(secret_key),
                service_id=str(service_id),
                merchant_id=str(merchant_id),
                phone_number=str(phone_number),
                user_id=str(user_merchant_id),
                order_id=str(order.pk),
                amount=amount
            )
            if invoice_id is None:
                payment_text += '(UZCLICK) Ошибка при выставлении счёта'
            else:
                payment_text += '(UZCLICK) Номер чека: {}'.format(invoice_id)
        except:
           payment_text += '(UZCLICK) Ошибка при выставлении счёта'
        bot.send_message(self.user_id, payment_text)
        self.send_channel(order)
        self.category()

    def check_order_products(self):
        self.new_step(8)
        total_price = 0
        text = "Ваш заказ:\n\n"
        location = False
        inline = InlineKeyboardMarkup()
        order = self.get_order()
        order_product = OrderProduct.objects.filter(order=order, status='order')
        text += "Телефон: " + str(order.user.phone) + '\n'
        if order.address is not None:
            text += '<b>' + 'Адрес: ' + str(order.address) + '</b>' + '\n\n'
        elif order.latitude:
            text += '<b>' + 'Адрес: Через локацию' + '</b>' + '\n\n'
            location = True
        text += 'Метод оплаты: ' + str(order.payment) + '\n'
        for i in order_product:
            price = i.quantity * i.product.price
            total_price += price
            text += '<b>' + str(i.product) + '</b>\n' + str(i.quantity) + ' * ' + str(i.product.price) + ' = ' + str(
                price) + '\n\n'
        text += '<b>Итого:' + str(total_price) + '</b>' + '\n'
        text += '+ Стоимость доставки:\n10000 сум'
        if location:
            link = 'http://maps.google.com?q=' + str(order.latitude) + ',' + str(order.longitude)
            inline.add(InlineKeyboardButton('Открыть адрес в карте🗺', url=link))
        inline.add(InlineKeyboardButton('❌', callback_data='cancel_order'),
                   InlineKeyboardButton('✅', callback_data='success_order'))
        bot.send_message(self.user, 'Если всё правильно нажмите ✅', reply_markup=ReplyKeyboardRemove())
        bot.send_message(self.user_id, text, reply_markup=inline, parse_mode='HTML')

    def success_orders(self):
        order = self.get_order()
        check = Order.objects.filter(user=self.user, status='processing').delete()
        order.status = 'accepted'
        order.save()
        bot.edit_message_text(chat_id=self.user_id, message_id=self.message_id,
                              text="Пожалуйста, дождитесь подтверждения")
        if order.payment == '💳 Click':
            self.pay_click(order)
        else:
            self.send_channel(order)
            self.category()

    def send_channel(self, order):
        text = ""
        total_price = self.order_total_amount(order)
        location = False
        inline = InlineKeyboardMarkup()
        order_product = OrderProduct.objects.filter(order=order, status='order')
        for i in order_product:
            text += '<b>' + str(i.product) + '</b>' + ' | x ' + str(i.quantity) + '\n'
        text += '\n<b>' + 'Итого: ' + str(total_price) + '</b>' + '\n\n'
        text += 'Метод оплаты: ' + str(order.payment) + '\n'
        text += "Оплата : " + '<b>Не оплачено</b>' + '\n'
        text += "Номер: " + str(order.user.phone) + '\n'
        if order.address is not None:
            text += '<b>' + 'Адрес: ' + str(order.address) + '</b>'
        elif order.latitude:
            text += '<b>' + 'Адрес: Через локацию' + '</b>'
            location = True
        if location:
            link = 'http://maps.google.com?q=' + str(order.latitude) + ',' + str(order.longitude)
            inline.add(InlineKeyboardButton('Открыть адрес в карте🗺', url=link))
        inline.add(InlineKeyboardButton('❌Отменить заказ', callback_data='no_order_kitchen%' + str(order.pk)),
                   InlineKeyboardButton('✅Принять заказ', callback_data='yes_order_kitchen%' + str(order.pk)))
        bot.send_message(self.channel_di, text, reply_markup=inline, parse_mode='HTML')

    def channel_update_status(self, order_id, message_id, accepted, call):
        get = Order.objects.get(pk=int(order_id))
        inline = InlineKeyboardMarkup()
        if accepted:
            get.status = 'processing'
            if get.latitude:
                link = 'http://maps.google.com?q=' + str(get.latitude) + ',' + str(get.longitude)
                inline.add(InlineKeyboardButton('Открыть адрес в карте🗺', url=link))
            update_text = f'Заказ #{str(order_id)}\n {call.message.text} \n' \
                f' <b>принят:</b><a href="tg://user?id={call.from_user.id}">{call.from_user.first_name}</a>'
            bot.edit_message_text(chat_id=self.channel_di,
                                  message_id=message_id,
                                  text=update_text,
                                  reply_markup=inline,
                                  parse_mode='html')
            send_text = str(get.user.first_name) + " Ваш заказ принят ✅"
            bot.send_message(get.user.user_id, send_text)
            get.save()
            self.send_delivery_channel(get)
        else:
            get.status = 'cancel'
            get.save()
            text_cancel = "Сожалеем, но ваш заказ отменен."
            bot.send_message(get.user.user_id, text_cancel)
            update_text = f'<b>Заказ #{str(order_id)} \n отклонен:</b>' \
                              f'<a href="tg://user?id={call.from_user.id}">{call.from_user.first_name}</a>',
            bot.edit_message_text(text=update_text,
                                  chat_id=self.channel_di,
                                  message_id=message_id,
                                  parse_mode='HTML',
                                  reply_markup=inline)

    def send_delivery_channel(self, order):
        text = ""
        total_price = self.order_total_amount(order)
        location = False
        inline = InlineKeyboardMarkup()
        order_product = OrderProduct.objects.filter(order=order, status='order')
        for i in order_product:
            text += '<b>' + str(i.product) + '</b>' + ' | x ' + str(i.quantity) + '\n'
        text += '\n<b>' + 'Итого: ' + str(total_price) + '</b>' + '\n\n'
        text += 'Метод оплаты: ' + str(order.payment) + '\n'
        text += "Оплата : " + '<b>Не оплачено</b>' + '\n'
        text += "Номер: " + str(order.user.phone) + '\n'
        if order.address is not None:
            text += '<b>' + 'Адрес: ' + str(order.address) + '</b>'
        elif order.latitude:
            text += '<b>' + 'Адрес: Через локацию' + '</b>'
            location = True
        if location:
            link = 'http://maps.google.com?q=' + str(order.latitude) + ',' + str(order.longitude)
            inline.add(InlineKeyboardButton('Открыть адрес в карте🗺', url=link))
        inline.add(InlineKeyboardButton('✅Принять заказ', callback_data='yes_order_driver%' + str(order.pk)))
        bot.send_message(self.deliver_channel_di, text, reply_markup=inline, parse_mode='HTML')

    def send_driver_confirmation(self, order_id, message_id, call):
        get = Order.objects.get(pk=int(order_id))
        inline = InlineKeyboardMarkup()
        get.status = 'delivering'
        if get.latitude:
            link = 'http://maps.google.com?q=' + str(get.latitude) + ',' + str(get.longitude)
            inline.add(InlineKeyboardButton('Открыть адрес в карте🗺', url=link))
        update_text = f'Доставка #{str(order_id)}\n {call.message.text} \n' \
            f' <b>принят:</b><a href="tg://user?id={call.from_user.id}">{call.from_user.first_name}</a>'
        bot.edit_message_text(chat_id=self.deliver_channel_di,
                              message_id=message_id,
                              text=update_text,
                              reply_markup=inline,
                              parse_mode='html')
        send_text = str(get.user.first_name) + "Ваш заказ в пути 🚗"
        bot.send_message(get.user.user_id, send_text)
        get.save()
        self.category()

    def order_total_amount(self, order):
        total_price = 0
        order_product = OrderProduct.objects.filter(order=order, status='order')
        for i in order_product:
            price = i.quantity * i.product.price
            total_price += price
        return total_price

    def delete_in_basket(self, product):
        sp = product.split('❌ ')
        order = self.get_order()
        order_product = OrderProduct.objects.filter(order=order, status='order', delete=sp[1]).delete()
        self.open_basket()

    def clear_basket(self):
        order = self.get_order()
        order_product = OrderProduct.objects.filter(order=order, status='order').delete()
        bot.send_message(self.user_id, 'Корзина очищена!')
        self.category()

    def check_int(self, number):
        try:
            int(number)
            if int(number) > 0:
                return True
            else:
                return False
        except:
            return False

    def get_category(self, category):
        return Category.objects.get(name=category)

    def get_order(self):
        get, create = Order.objects.get_or_create(user=self.user, status='new')
        return get

    def get_order_product(self):
        get, create = OrderProduct.objects.get_or_create(order=self.get_order(), status='new')
        return get

    def basket_btn(self):
        user_basket = OrderProduct.objects.filter(order=self.get_order(), status='order').count()
        button = KeyboardButton("🛒Корзина" + '(' + str(user_basket) + ')')
        return button

    def back_btn(self):
        back_btn = KeyboardButton('Назад ↩️')
        return back_btn

    def new_step(self, step):
        self.user.step = int(step)
        self.user.save()
