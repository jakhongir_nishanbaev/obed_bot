from django.db import models
from django.utils.timezone import localdate


STATUS_ORDER = (
    ('new',    'new',),
    ('accepted', 'accepted'),
    ('processing', 'processing'),
    ('delivering',   'delivering'),
    ('cancel', 'cancel'),
)


class Order(models.Model):
    user = models.ForeignKey('telegram.TelegramUser', on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_ORDER, max_length=50, default='new')
    latitude = models.CharField(max_length=255, blank=True, null=True)
    longitude = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    payment = models.CharField(max_length=50, null=True)
    created = models.DateField(auto_now_add=localdate())

    def __str__(self):
        return '{} | {}'.format(str(self.user), self.status)


STATUS_ORDER_PRODUCT = (('new', 'new'), ('order', 'order'))


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_ORDER_PRODUCT, default='new', max_length=50)
    category = models.CharField(max_length=50, blank=True, null=True)
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE, null=True)
    delete = models.CharField(max_length=255, blank=True, null=True)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return '{} | {}'.format(str(self.order.user), self.status)