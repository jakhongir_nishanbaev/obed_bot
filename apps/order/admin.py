from django.contrib import admin

#model
from .models import Order,OrderProduct

admin.site.register(Order)
admin.site.register(OrderProduct)