from django.db import models


class Product(models.Model):
    photo = models.ImageField(upload_to='product')
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=400)
    command_name = models.CharField(max_length=50, blank=True, null=True)
    price = models.IntegerField(default=0)
    is_active = models.BooleanField(verbose_name="Показать клиентам?", default=True)

    def __str__(self):
        return self.name